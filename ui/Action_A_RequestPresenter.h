#pragma once

#include <QObject>
#include <QVariant>

#include "app/ActionRequestInterfaces.h"

class Action_A_RequestPresenter
        : public QObject
        , private IAction_A_Request
{
    Q_OBJECT

    Q_PROPERTY(int state READ state NOTIFY stateChanged)
    Q_PROPERTY(int value READ value NOTIFY valueChanged)
    Q_PROPERTY(QString info READ info NOTIFY infoChanged)

public:
    enum State
    {
        State_None,
        State_Value,
        State_Approve
    };
    Q_ENUM(State)

    explicit Action_A_RequestPresenter(QObject* parent = nullptr);

    int value() const { return m_value; }
    QString info() const { return m_info; }

    State state() const { return m_state; }
    void setState(State value);

    Q_INVOKABLE void apply(QVariant value);
    Q_INVOKABLE void approve(bool value);

private: // IAction_A_Request
    void retrieveIntValue(int current, std::function<void(int)>&& callback) override;
    void retrieveApproval(QString description, std::function<void(bool)>&& callback) override;
    void onRequestInterruption() override;

private:
    QString m_info;
    int m_value = 0;
    State m_state = State_None;
    std::function<void(int)> m_valueCallback;
    std::function<void(bool)> m_approveCallback;

signals:
    void stateChanged();
    void valueChanged();
    void infoChanged();

};
