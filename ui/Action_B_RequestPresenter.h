#pragma once

#include <QObject>

#include "app/ActionRequestInterfaces.h"

class Action_B_RequestPresenter
        : public QObject
        , private IAction_B_Request
{
    Q_OBJECT

    double m_value = 0;
    bool m_active = false;
    std::function<void(double)> m_callback;

    Q_PROPERTY(bool active READ active NOTIFY activeChanged)
    Q_PROPERTY(double value READ value NOTIFY valueChanged)

public:
    explicit Action_B_RequestPresenter(QObject* parent = nullptr);

    bool active() const { return m_active; }
    void setActive(bool value);

    double value() const { return m_value; }
    Q_INVOKABLE void setValue(double value);

private: // IAction_B_Request
    void retrieveDoubleValue(double current, std::function<void(double)>&& callback) override;
    void onRequestInterruption() override;

signals:
    void activeChanged();
    void valueChanged();

};
