#include "Action_A_RequestPresenter.h"

Action_A_RequestPresenter::Action_A_RequestPresenter(QObject* parent)
    : QObject(parent)
{
}

void Action_A_RequestPresenter::setState(State value)
{
    if (m_state != value)
    {
        m_state = value;
        emit stateChanged();
    }
}

void Action_A_RequestPresenter::apply(QVariant value)
{
    setState(State_None);

    if (m_valueCallback)
    {
        m_valueCallback(value.toInt());
        m_valueCallback = nullptr;
    }
}

void Action_A_RequestPresenter::approve(bool value)
{
    setState(State_None);

    if (m_approveCallback)
    {
        m_approveCallback(value);
        m_approveCallback = nullptr;
    }
}

void Action_A_RequestPresenter::retrieveIntValue(int current, std::function<void(int)>&& callback)
{
    m_valueCallback = std::move(callback);
    m_approveCallback = nullptr;

    m_info = "Enter value";
    emit infoChanged();

    m_value = current;
    emit valueChanged();

    setState(State_Value);
}

void Action_A_RequestPresenter::retrieveApproval(QString description, std::function<void(bool)>&& callback)
{
    m_valueCallback = nullptr;
    m_approveCallback = std::move(callback);

    m_info = description;
    emit infoChanged();

    setState(State_Approve);
}

void Action_A_RequestPresenter::onRequestInterruption()
{
    m_valueCallback = nullptr;
    m_approveCallback = nullptr;

    setState(State_None);
}
