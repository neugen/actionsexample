#pragma once

#include <QObject>

class Action_C_RequestPresenter
        : public QObject
{
    Q_OBJECT

    bool m_active = false;

    Q_PROPERTY(bool active READ active NOTIFY activeChanged)

public:
    explicit Action_C_RequestPresenter(QObject* parent = nullptr);

    bool active() const { return m_active; }
    void setActive(bool value);

    Q_INVOKABLE void justDoIt();

signals:
    void activeChanged();

};
