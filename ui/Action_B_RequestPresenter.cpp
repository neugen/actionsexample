#include "Action_B_RequestPresenter.h"

Action_B_RequestPresenter::Action_B_RequestPresenter(QObject* parent)
    : QObject(parent)
{
}

void Action_B_RequestPresenter::setActive(bool value)
{
    if (m_active != value)
    {
        m_active = value;
        emit activeChanged();
    }
}

void Action_B_RequestPresenter::setValue(double value)
{
    setActive(false);

    if (!qFuzzyCompare(m_value, value))
    {
        m_value = value;
        emit valueChanged();
    }

    if (m_callback)
    {
        m_callback(value);
        m_callback = nullptr;
    }
}

void Action_B_RequestPresenter::retrieveDoubleValue(double current, std::function<void(double)>&& callback)
{
    m_callback = std::move(callback);

    m_value = current;
    emit valueChanged();

    setActive(true);
}

void Action_B_RequestPresenter::onRequestInterruption()
{
    m_callback = nullptr;
    setActive(false);
}
