#pragma once

#include <map>
#include <functional>

#include "ActionRequest.h"

class ActionRequestRegistry : private ActionRequestStorage
{
    ActionRequest* m_lastRequest = nullptr;
    std::map<ActionRequest::Id, ActionRequest*> m_data;

public:
    class MaybeWithout
    {
        const bool c_valid;

    public:
        explicit MaybeWithout(bool valid)
            : c_valid(valid) {}

        void without(std::function<void()>&& callback) const
        {
            if (c_valid)
                callback();
        }
    };

    template <typename TActionRequestImpl>
    void attach()
    {
        TActionRequestImpl::attach(static_cast<ActionRequestStorage*>(this));
    }

    template <typename TActionRequestImpl>
    MaybeWithout with(std::function<void(TActionRequestImpl*)>&& callback)
    {
        const auto it = m_data.find(TActionRequestImpl::ID);

        if (it == m_data.end())
            return MaybeWithout(true);

        if (m_lastRequest && m_lastRequest->isInterruptible())
            m_lastRequest->onRequestInterruption();

        m_lastRequest = it->second;

        auto* impl = static_cast<TActionRequestImpl*>(m_lastRequest);
        callback(impl);

        return MaybeWithout(false);
    }

private: // ActionRequestRegistryStorage
    void add(ActionRequest* request) override;
    void remove(ActionRequest* request) override;

};
