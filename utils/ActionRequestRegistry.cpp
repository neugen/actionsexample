#include "ActionRequestRegistry.h"

void ActionRequestRegistry::add(ActionRequest* request)
{
    m_data.insert(std::make_pair(request->id(), request));
}

void ActionRequestRegistry::remove(ActionRequest* request)
{
    auto it = m_data.find(request->id());

    if (it != m_data.end())
        m_data.erase(it);
}
