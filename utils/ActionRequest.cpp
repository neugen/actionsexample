#include "ActionRequest.h"

namespace
{
static ActionRequest::Id s_lastId = 0;

} // anonymus namespace

ActionRequest::Id ActionRequest::makeId()
{
    return ++s_lastId;
}
