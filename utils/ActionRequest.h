#pragma once

class ActionRequest
{
protected:
    ~ActionRequest() = default;

public:
    using Id = int;

    virtual Id id() const = 0;
    virtual bool isInterruptible() const = 0;
    virtual void onRequestInterruption() {}

protected:
    static Id makeId();

};

class ActionRequestStorage
{
protected:
    ~ActionRequestStorage() = default;

public:
    virtual void add(ActionRequest* request) = 0;
    virtual void remove(ActionRequest* request) = 0;
};
