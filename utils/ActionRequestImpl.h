#pragma once

#include "ActionRequest.h"

template <typename T>
class ActionRequestImpl : protected ActionRequest
{
    friend class ActionRequestRegistry;

    static const Id ID;
    static ActionRequestStorage* s_storage;

public:
    explicit ActionRequestImpl()
    {
        assert(s_storage && "interface was not attached!");
        s_storage->add(this);
    }

    ~ActionRequestImpl()
    {
        s_storage->remove(this);
    }

private: // ActionRequest
    Id id() const override { return ID; }
    bool isInterruptible() const override { return false; }

private:
    static void attach(ActionRequestStorage* storage)
    {
        assert(!s_storage && "interface is already attached!");
        s_storage = storage;
    }

};

template <typename T>
class InterruptibleActionRequestImpl : public ActionRequestImpl<T>
{
public:
    virtual void onRequestInterruption() = 0;

private: // ActionRequest
    bool isInterruptible() const override { return true; }

};


template <typename T>
const ActionRequest::Id ActionRequestImpl<T>::ID = ActionRequest::makeId();

template <typename T>
ActionRequestStorage* ActionRequestImpl<T>::s_storage = nullptr;
