#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QQmlEngine>
#include <QJSEngine>

#include "app/System.h"
#include "app/Payload.h"
#include "app/ActionsDispatcher.h"
#include "ui/Action_A_RequestPresenter.h"
#include "ui/Action_B_RequestPresenter.h"
#include "ui/Action_C_RequestPresenter.h"

QObject* systemSingletonePtr = nullptr;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    System system;
    {
        systemSingletonePtr = &system;
        QQmlEngine::setObjectOwnership(systemSingletonePtr, QQmlEngine::CppOwnership);

        qmlRegisterSingletonType<System>("Test", 1, 0, "System", [](auto*, auto*)
        {
            return systemSingletonePtr;
        });

        qmlRegisterUncreatableType<Payload>("Test", 1, 0, "Payload", "");
        qmlRegisterUncreatableType<ActionsDispatcher>("Test", 1, 0, "ActionsDispatcher", "");

        qmlRegisterType<Action_A_RequestPresenter>("Test", 1, 0, "Request_A_Presenter");
        qmlRegisterType<Action_B_RequestPresenter>("Test", 1, 0, "Request_B_Presenter");
        qmlRegisterType<Action_C_RequestPresenter>("Test", 1, 0, "Request_C_Presenter");
    }

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl)
    {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.load(url);

    return app.exec();
}
