#include "ActionsDispatcher.h"

#include "utils/ActionRequestRegistry.h"
#include "ActionRequestInterfaces.h"

struct ActionsDispatcherData
{
    Payload* payload = nullptr;
    ActionRequestRegistry registry;

    explicit ActionsDispatcherData()
    {
        registry.attach<IAction_A_Request>();
        registry.attach<IAction_B_Request>();
    }
};

ActionsDispatcher::ActionsDispatcher(QObject* parent)
    : QObject(parent)
    , p(std::make_unique<ActionsDispatcherData>())
{
}

ActionsDispatcher::~ActionsDispatcher()
{
}

void ActionsDispatcher::setPayload(Payload* value)
{
    p->payload = value;
}

void ActionsDispatcher::doAction_A()
{
    p->registry.with<IAction_A_Request>([this](auto* impl)
    {
        const int current = int(p->payload->value());

        impl->retrieveIntValue(current, [this, impl](int value)
        {
            if (value < 0 || value > 100)
            {
                impl->retrieveApproval(
                            "Value is outside of bounds [0..100]",
                            [this, value](bool approved)
                {
                    if (approved)
                        p->payload->setValue(value);
                });
            }
            else
            {
                p->payload->setValue(value);
            }
        });

    }).without([this]()
    {
        p->payload->setValue(12345.6789);
    });
}

void ActionsDispatcher::doAction_B()
{
    p->registry.with<IAction_B_Request>([this](auto* impl)
    {
        const int current = int(p->payload->value());

        impl->retrieveDoubleValue(current, [this](double value)
        {
            p->payload->setValue(value);
        });

    }).without([this]()
    {
        p->payload->setValue(9876.54321);
    });
}
