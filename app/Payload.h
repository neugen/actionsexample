#pragma once

#include <QObject>

class Payload : public QObject
{
    Q_OBJECT

    double m_value = 0;

    Q_PROPERTY(double value READ value NOTIFY valueChanged)

public:
    explicit Payload(QObject* parent = nullptr);

    double value() const { return m_value; }
    void setValue(double value);

signals:
    void valueChanged();
};

