#include "Payload.h"

Payload::Payload(QObject* parent)
    : QObject(parent)
{
}

void Payload::setValue(double value)
{
    if (!qFuzzyCompare(m_value, value))
    {
        m_value = value;
        emit valueChanged();
    }
}
