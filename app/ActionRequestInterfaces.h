#pragma once

#include <functional>

#include <QString>

#include "utils/ActionRequestImpl.h"


class IAction_A_Request : public InterruptibleActionRequestImpl<IAction_A_Request>
{
public:
    virtual void retrieveIntValue(int current, std::function<void(int)>&& callback) = 0;
    virtual void retrieveApproval(QString description, std::function<void(bool)>&& callback) = 0;
};



class IAction_B_Request : public InterruptibleActionRequestImpl<IAction_B_Request>
{
public:
    virtual void retrieveDoubleValue(double current, std::function<void(double)>&& callback) = 0;
};



class IAction_C_Request : public ActionRequestImpl<IAction_C_Request>
{
public:
    virtual void retrieveAction(std::function<void()>&& callback) = 0;
};
