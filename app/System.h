#pragma once

#include <QObject>

#include <memory>

class Payload;
class ActionsDispatcher;

class System : public QObject
{
    Q_OBJECT

    std::unique_ptr<struct SystemData> p;

    Q_PROPERTY(Payload* payload READ payload CONSTANT)
    Q_PROPERTY(ActionsDispatcher* dispatcher READ dispatcher CONSTANT)

public:
    explicit System(QObject* parent = nullptr);
    ~System();

    Payload* payload() const;
    ActionsDispatcher* dispatcher() const;

};
